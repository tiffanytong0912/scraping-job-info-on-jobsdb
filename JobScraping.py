import requests
from bs4 import BeautifulSoup, NavigableString, Tag
import re
import pandas as pd
import os.path

#Create job information's lists
titleList = []
companyList = []
locationList = []
highlightsList = []
linkList = []
jobDescriptionList = []

def urlHandling(keyword, page):
    """ Handle URL for Request flight information from job searching websites according to keyword
    https://hk.jobsdb.com/hk/search-jobs/data-engineer/1

    Args:
        keyword (str): The keyword you want to query, "data-engineer", "data-engineering", "junior-data-engineer"
        page (int): The page you want to query, 1

    Returns:
        Request of the URL
    """

    query1 = "https://hk.jobsdb.com/hk/search-jobs/"
    keyword = keyword.lower().replace(" ", "-")

    url = query1 + keyword + "/" + str(page)

    print(url)
    return requests.get(url)

def scrapingJobsData(req, keyword):
    """ Scraping jobs informations from Jobsdb according to keyword searching in one page

    Args:
        req (response): The response of the URL
        keyword (str): The keyword you want to query, "data-engineer", "data-engineering", "junior-data-engineer"
    """
    keyword = keyword.lower().replace(" ", "-")

    soup = BeautifulSoup(req.content, 'html.parser')

    #Find out the jobs list on the Jobsdb
    jobListing = soup.select_one('[data-automation="jobListing"]')
    #The number of the job item in this page
    itemNo = 0

    #Scrape job informations one by one and add the details in those lists created before
    for titleItem in jobListing:
        jobItem = jobListing.select_one('[data-automation="job-card-{}"]'.format(itemNo))

        #Set items to be none first
        titleItem = 'None'
        companyItem = 'None'
        locationItem = 'None'
        highlightsItem = 'None'
        linkItem = 'None'

        #Find the details of the job
        try:
            titleItem = jobItem.find('h1', attrs={"class":"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0"}).text
            companyItem = jobItem.find('span', attrs={"class":"FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0"}).text
            locationItem = jobItem.find('span', attrs={"class":"FYwKg _3MPd_ _2Bz3E And8z"}).text
            highlightsItem = jobItem.find_all('span', attrs={"class":"FYwKg _2Bz3E C6ZIU_0 _1_nER_0 _2DNlq_0 _29m7__0 _1PM5y_0"})
            linkItem = jobItem.find('a')['href']
        except:
            pass

        #Append the detail informations into the lists
        titleList.append(titleItem)
        companyList.append(companyItem)
        locationList.append(locationItem)
        linkItem = 'https://hk.jobsdb.com/'+linkItem
        linkList.append(linkItem)

        #Check if there is highlight description or not and set the count number
        highlightPointList = []
        if highlightsItem == 'None':
            highlightPointCount = 0
        else:
            highlightPointCount = len(highlightsItem)
        
        #To sperate the text of the highlight description point by point
        for highlightPointItem in range(0, highlightPointCount):
            highlightPointList.append(highlightsItem[highlightPointItem].text)
            highlightsList.append(highlightPointList)

        itemNo += 1

def scrapJobDetails(url):
    """ Scraping jobs description from Jobsdb according to each job

    Args:
        url (str): Each job details website
    """
    req = requests.get(url)
    soup = BeautifulSoup(req.content, 'html.parser')
    jobDescription = soup.select_one('[data-automation="jobDescription"]')

    jobDescriptionItem = 'None'
    try:
        jobDescriptionItem = jobDescription.find('div',attrs={"class":"FYwKg"})
    except:
        pass

    jobDescriptionItemCount = 0
    jobDescriptionPointList = []
    #To separate the text of the job description point by point
    for jobDescriptionPointItem in jobDescriptionItem.find_all('span'): 
        jobDescriptionPointItemText = jobDescriptionPointItem.text
        if jobDescriptionItemCount > 0:
            #It has a bug that will repeat the scraping in same text, so first check if it is repeat or not
            if jobDescriptionPointItemText == jobDescriptionPointList[jobDescriptionItemCount - 1]:
                jobDescriptionItemCount -= 1
            else:
                jobDescriptionPointList.append(jobDescriptionPointItemText)
        else:
            jobDescriptionPointList.append(jobDescriptionPointItemText)
        
        jobDescriptionItemCount += 1

    jobDescriptionList.append(jobDescriptionPointList)
    #print(jobDescriptionList)
    print(url)

def saveToCSV(keyword):
    """Transfrom those lists to dataframe and save to CSV

    Args:
        keyword (str): Save the CSV named by keyword
    """
    keyword = keyword.lower().replace(" ", "-")
    #print(len(titleList))
    
    jobList = []
    itemNo = 0
    #Put into dataframe
    for item in titleList:
        jobList.append([titleList[itemNo], companyList[itemNo], locationList[itemNo], 
                        highlightsList[itemNo], linkList[itemNo], jobDescriptionList[itemNo]])
        itemNo += 1
    job_df = pd.DataFrame(jobList)
    job_df.columns = ['job_title', 'company', 'location', 'highlights', 'details_link', 'job_description']

    #Create file if not exist
    date = pd.to_datetime('today').date()
    if os.path.isfile('{}_FromJobsDB_{}.csv'.format(keyword, date)):
        job_df.to_csv('{}_FromJobsDB_{}.csv'.format(keyword, date), mode='a',index = False, header = False)
    else:
        job_df.to_csv('{}_FromJobsDB_{}.csv'.format(keyword, date),index = False)

    #Clear all the lists
    titleList.clear()
    companyList.clear()
    locationList.clear()
    highlightsList.clear()
    linkList.clear()
    jobDescriptionList.clear()

def countPage(req):
    """ Count the total page number of the result searching by keyword

    Args:
        req (response): The response of the URL

    Returns:
        pages (int): The total page number
    """
    soup = BeautifulSoup(req.content, 'html.parser')
    #Find the total jobs count searching by the keyword
    total_jobs = soup.find('span', {'class': 'FYwKg _2Bz3E C6ZIU_0 _1_nER_0 _2DNlq_0 _29m7__0 _1PM5y_0'}).text.split()[2]
    #Replace the comma in the number and make it can be calculate
    total_jobs = total_jobs.replace(",", "")
    #Each page has 30 items and plus one page for the remain items
    pages = round(float(total_jobs)/30) + 1
    #print(pages)
    return pages

def main():
    """Main function
    """
    #First scape the data of page 1 and scape the total job items count for calculate the total pages
    page = 1
    keyword = "Data Engineer"
    req = urlHandling(keyword, page)
    scrapingJobsData(req, keyword)
    total_page = countPage(req)

    #Start the while loop on page 2
    page = 2
    while(page <= total_page) :
        req = urlHandling(keyword, page)
        scrapingJobsData(req, keyword)
        page += 1

    #After sracping data of the job cards, then start to scrape the desciptions of each job
    linkItemCount = 0
    for item in linkList:
        scrapJobDetails(linkList[linkItemCount])
        linkItemCount += 1

    #Save those data to CSV file
    saveToCSV(keyword)

    #Finish all scraping and saving
    print("Finish")

if __name__ == "__main__":
    main()